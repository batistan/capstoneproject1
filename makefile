CC=nvcc
CFLAGS=-I /usr/local/cuda/samples/common/inc

# default
all: apsp_template.cu
	$(CC) $(CFLAGS) -o apsp apsp_template.cu

debug: apsp_template.cu
	$(CC) -g $(CFLAGS) -o apsp apsp_template.cu

matrixgen: matrixgen.cpp
	$(CC) $(CFLAGS) -o matrixgen matrixgen.cpp

# find files of type f (rather than type d for directories) in current directory (no subdirectories) without .c anywhere in the filename (since our source files are .cu, .csv, .cpp
# execute rm -i on every file found
# this is less efficient than using the -delete flag, since it spawns a separate process for each file matched, but it lets us confirm deletion
clean:
	# nvm
	#find . -maxdepth 1 -type f -not -name '*.c*' -not -name 'makefile' -exec rm -i {} \;

//to compile: nvcc  apsp_ref_example.cu  -I /usr/local/cuda/samples/common/inc/ -o apsp_ref_example 
//to run (e.g.): ./apsp_ref_example 

#include <cassert>
#include <stdio.h>
#include <math.h>
#include <cuda.h>
#include <helper_cuda.h>
#include <map>
#include <algorithm>
#include <iostream>
#include <string>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/iterator/zip_iterator.h>

using namespace std;
	
static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

class cuda_timer
{
private:
	cudaEvent_t start;
	cudaEvent_t end;
	cudaStream_t s;
	cudaError_t error;
public:	
	cuda_timer(cudaStream_t _s):s(_s)
	{
	    error =cudaEventCreate(&start); 
	    assert(error==cudaSuccess);
	    error =cudaEventCreate(&end);
	    assert(error==cudaSuccess);
	    error = cudaEventRecord(start,s);
	    assert(error==cudaSuccess);
	}

	float display()
	{
	    float elapsed_time;
	    error=cudaEventRecord(end, s);
	    assert(error==cudaSuccess);
	    error=cudaEventSynchronize(end);
	    assert(error==cudaSuccess);
	    error=cudaEventElapsedTime(&elapsed_time, start, end);
	    assert(error==cudaSuccess);
	    return elapsed_time;
	  }
	  ~cuda_timer()
	  {
	     cudaEventDestroy( start );
	     cudaEventDestroy( end );	  
	  }
};

__global__ void InitMatrixKernel(float* Md, float val,int size)
{
    int tidx = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;
    for(; tidx < size; tidx += stride)
        Md[tidx] = val;	
}


__global__ void MatrixMulKernel(float* Md, float* Nd, float* Pd, int Width,int Tile_Width)
{
  //__shared__ float Mds[Tile_Width][Tile_Width];
  //__shared__ float Nds[Tile_Width][Tile_Width];
  extern __shared__ float buff[];
  float *Mds=&buff[0*Tile_Width*Tile_Width];
  float *Nds=&buff[1*Tile_Width*Tile_Width];


  int bx = blockIdx.x;  int by = blockIdx.y;
  int tx = threadIdx.x; int ty = threadIdx.y;

// Identify the row and column of the Pd element to work on
  int Row = by * Tile_Width + ty;
  int Col = bx * Tile_Width + tx;

  float Pvalue = Pd[Row*Width+Col];
  // Loop over the Md and Nd tiles required to compute the Pd element
   for (int m = 0; m < Width/Tile_Width; ++m) 
   {
	// Coolaborative loading of Md and Nd tiles into shared memory
	  //Mds[ty][tx] = Md[Row*Width + (m*Tile_Width + tx)];
	  //Nds[ty][tx] = Nd[Col + (m*Tile_Width + ty)*Width];
	  Mds[ty*Tile_Width+tx] = Md[Row*Width + (m*Tile_Width + tx)];
	  Nds[ty*Tile_Width+tx] = Nd[Col + (m*Tile_Width + ty)*Width];
	  __syncthreads();
	 
	 for (int k = 0; k < Tile_Width; ++k)
	 {	  
		  //if(Pvalue > Mds[ty][k] + Nds[k][tx])
		  //	Pvalue = Mds[ty][k] + Nds[k][tx];
		if(Pvalue > Mds[ty*Tile_Width+k] + Nds[k*Tile_Width+tx])
		Pvalue = Mds[ty*Tile_Width+k] + Nds[k*Tile_Width+tx];
		 __syncthreads();
	}
    }
   Pd[Row*Width+Col] = Pvalue;
}

struct func
{
  __device__ float operator()(thrust::tuple<float, float> t)
  {
     float f = thrust::get<0>(t) - thrust::get<1>(t);
     return f*f;
  }
};

void apsp_cpu(float *v, int Width)
{
	for(int k=0;k<Width;k++)
		for(int i=0;i<Width;i++)
			for(int j=0;j<Width;j++)
			{
				float d=v[i*Width+k]+v[k*Width+j];
				if(d<v[i*Width+j])
					v[i*Width+j]=d;
			}
					
}

int main( int argc,char **argv ) 
{
   int Width=8;
   int Tile_Width=8;
   int size=Width*Width;
   printf("width=%d Tile_Width=%d size=%d\n",Width,Tile_Width,size);
   float Mh[]={0,2,4,0,0,0,0,0,
   	2,0,3,4,0,0,0,0,
   	4,3,0,1,3,0,0,0,
   	0,4,1,0,3,3,1,0,
   	0,0,3,3,0,2,0,0,
   	0,0,0,3,2,0,6,0,
   	0,4,0,1,0,6,0,114,
   	0,2,0,0,0,0,14,0};	
    float Nh[size];
    for(int i=0;i<size;i++)
    	if((Mh[i]==0)&&(i%Width!=i/Width)) Mh[i]=1e+5;
  
  // allocate the memory on the GPU
    float* Md=NULL;
    float* Nd=NULL;
    float* Pd=NULL;
    HANDLE_ERROR( cudaMalloc( (void**)&Md,size* sizeof(float) ) );
    HANDLE_ERROR( cudaMalloc( (void**)&Nd, size* sizeof(float) ) );
    HANDLE_ERROR( cudaMalloc( (void**)&Pd, size* sizeof(float) ) );

    // copy the arrays 'Mh' and 'Nh' to the GPU
    HANDLE_ERROR( cudaMemcpy(Md, Mh, size * sizeof(float),  cudaMemcpyHostToDevice ) );
    
    InitMatrixKernel<<<size/Tile_Width/Tile_Width,Tile_Width*Tile_Width>>>(Pd,1e+5,size);
    getLastCudaError("InitMatrixKernel() execution failed.\n");
    HANDLE_ERROR(cudaMemcpy(Nd, Md, size* sizeof(float),cudaMemcpyDeviceToDevice));

    dim3   DimGrid(Width/Tile_Width,Width/Tile_Width);  
    dim3   DimBlock(Tile_Width,Tile_Width); 
    for(int i=0;i<Width;i++)
    {
    	printf("loop %d\n",i);
    	MatrixMulKernel<<<DimGrid,DimBlock,2*Tile_Width*Tile_Width*sizeof(float)>>>( Md, Nd, Pd, Width,Tile_Width);
    	cudaDeviceSynchronize();
    	
    	getLastCudaError("MatrixMulKernel() execution failed.\n");
    	thrust::device_ptr<float> pv_ptr = thrust::device_pointer_cast(Pd);
    	thrust::device_ptr<float> nv_ptr = thrust::device_pointer_cast(Nd);
    	
    	thrust::zip_iterator<thrust::tuple<thrust::device_ptr<float>, thrust::device_ptr<float> > > begin =
    		thrust::make_zip_iterator(thrust::make_tuple(pv_ptr, nv_ptr));
    	thrust::zip_iterator<thrust::tuple<thrust::device_ptr<float>, thrust::device_ptr<float> > > end = 
    		thrust::make_zip_iterator(thrust::make_tuple(pv_ptr+size, nv_ptr+size));
    	float diff_square = thrust::transform_reduce(begin,end,func(),0.0f, thrust::plus<float>());
	printf("i=%d diff=%20.5f\n",i, sqrt(diff_square));                        
	
	//copy Pd to Nd
	HANDLE_ERROR(cudaMemcpy(Nd, Pd, size* sizeof(float),cudaMemcpyDeviceToDevice));
        // copy the array 'Nd' back from the GPU to the CPU
        HANDLE_ERROR( cudaMemcpy( Nh, Nd, size* sizeof(float),cudaMemcpyDeviceToHost ) );	
        
        for(int s=0;s<Width;s++)
        {
        	for(int t=0;t<Width;t++)
        		printf("%10.2f",Nh[s*Width+t]);
        	printf("\n");
        }
        if(diff_square<1e-5) break;
    }    

if(1)
{
	 int num_diff=0;
	 float* v=new float[size];
	 assert(v!=NULL);
	 for(int i=0;i<size;i++)
	 	v[i]=((Mh[i]==0)&&(i%Width!=i/Width))?1e+5:Mh[i];
	 apsp_cpu(v,Width);
	 printf("\n\nCPU Result\n");
	 for(int i=0;i<size;i++)
	 {
	 	if(v[i]!=Nh[i])num_diff++;
	 	printf("%8.2f",v[i]);
	 	if(i%Width==Width-1) printf("\n");
	 		
	 }
	 printf("num_diff=%d\n",num_diff);	 	
}
    
    // free the memory allocated on the GPU
    HANDLE_ERROR( cudaFree( Md ) );
    HANDLE_ERROR( cudaFree( Nd ) );
    HANDLE_ERROR( cudaFree( Pd ) );
    
    return 0;
}

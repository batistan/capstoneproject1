//to compile: nvcc  apsp_template.cu  -I /usr/local/cuda/samples/common/inc/ -o apsp
//to run (e.g.): ./apsp 

#include <algorithm>
#include <fstream>
#include <cassert>
#include <stdio.h>
#include <math.h>
#include <cuda.h>
#include <map>
#include <string>
#include <helper_cuda.h>
#include <thrust/host_vector.h>
#define TILE_WIDTH 16

using namespace std;
class cuda_map {
    
};
static void HandleError( cudaError_t err,
	const char *file,
	int line ) {
  if (err != cudaSuccess) {
    printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
		file, line );
    exit( EXIT_FAILURE );
  }

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

class cuda_timer
{
  private:
	cudaEvent_t start;
	cudaEvent_t end;
	cudaStream_t s;
	cudaError_t error;
  public:	
	cuda_timer(cudaStream_t _s):s(_s)
  {
	error =cudaEventCreate(&start); 
	assert(error==cudaSuccess);
	error =cudaEventCreate(&end);
	assert(error==cudaSuccess);
	error = cudaEventRecord(start,s);
	assert(error==cudaSuccess);
  }

	float display()
	{
	  float elapsed_time;
	  error=cudaEventRecord(end, s);
	  assert(error==cudaSuccess);
	  error=cudaEventSynchronize(end);
	  assert(error==cudaSuccess);
	  error=cudaEventElapsedTime(&elapsed_time, start, end);
	  assert(error==cudaSuccess);
	  return elapsed_time;
	}
	~cuda_timer()
	{
	  cudaEventDestroy( start );
	  cudaEventDestroy( end );	  
	}
};

__global__ void InitMatrixKernel(float* Md, float val,int size)
{
  int tidx = threadIdx.x + blockDim.x * blockIdx.x;
  int stride = blockDim.x * gridDim.x;
  for(; tidx < size; tidx += stride)
	Md[tidx] = val;	
}


__global__ void MatrixMulKernel(float* Md, float* Nd, float* Pd, int Width )
{
  __shared__ float Mds[TILE_WIDTH][TILE_WIDTH];
  __shared__ float Nds[TILE_WIDTH][TILE_WIDTH];

  int bx = blockIdx.x;  int by = blockIdx.y;
  int tx = threadIdx.x; int ty = threadIdx.y;

  // Identify the row and column of the Pd element to work on
  int Row = by * TILE_WIDTH + ty;
  int Col = bx * TILE_WIDTH + tx;

  float Pvalue = Pd[Row*Width+Col];
  // Loop over the Md and Nd tiles required to compute the Pd element
  for (int m = 0; m < Width/TILE_WIDTH; ++m) 
  {
	// Coolaborative loading of Md and Nd tiles into shared memory
	Mds[ty][tx] = Md[Row*Width + (m*TILE_WIDTH + tx)];
	Nds[ty][tx] = Nd[Col + (m*TILE_WIDTH + ty)*Width];
	__syncthreads();
	// your code for task2 goes here
	__syncthreads();
  }
  Pd[Row*Width+Col] = Pvalue;
}

int main( int argc,char **argv ) 
{
  if(argc!=2)
  {
	printf("%s weight-file\n",argv[0]);
	exit(-1);
  }

	map<string,int> nodemap;
	fstream datafile(argv[1]);
	assert(datafile!=NULL);
	string line,from,to,str;
	double w;
	int len=0,seq=0;
	while(!datafile.eof())
	{
	getline(datafile,from,',');
	if(from.length()==0) break;
	getline(datafile,to,',');
	getline(datafile,str);
		//cout<<from<<","<<to<<","<<str<<endl;
		if(nodemap.find(from)==nodemap.end())
			nodemap[from]=(seq++);//Nodemap["0012245"]=1;
		if(nodemap.find(to)==nodemap.end())
			nodemap[to]=(seq++);//Nodemap["0064133"]=2;
		len++;
	}
	printf("len=%d seq=%d\n",len,seq);
	datafile.close();
	int Level=ceil(log(seq)/log(2));
	int Width=pow(2,Level);

  //determine size 
  int size=Width * Width;
  // allocate the memory on the CPU        
  float* Mh=new float[size];
  float* Nh=new float[size];
  assert(Mh!=NULL&&Nh!=NULL);            

  datafile.open(argv[1]);
		for(int i=0;i<len;i++)
		{
			getline(datafile,from,',');
			getline(datafile,to,',');
			getline(datafile,str);
			w=atof(str.c_str());
			//cout<<from<<","<<to<<","<<w<<endl;
			int fi=nodemap[from];
			int ti=nodemap[to];
			if(fi>=Width||ti>=Width) continue;
			//printf("fi=%d ti=%d\n",fi,ti);
			Mh[fi*Width+ti]=w;
			Mh[fi*Width+fi]=0;
			Mh[ti*Width+ti]=0;
		}
  
  // allocate the memory on the GPU
  float* Md=NULL;
  float* Nd=NULL;
  HANDLE_ERROR( cudaMalloc( (void**)&Md,size* sizeof(float) ) );
  HANDLE_ERROR( cudaMalloc( (void**)&Nd, size* sizeof(float) ) );

  // copy the arrays 'Mh' and 'Nh' to the GPU
  HANDLE_ERROR( cudaMemcpy( Md, Mh, size * sizeof(float),
		cudaMemcpyHostToDevice ) );

  //your code on calculating Level goes here (task 2); note that Level=1 is a placeholder in order to compile
  //int Level=1;
  //int Width=pow(2,Level);	    
  printf("Width=%d\n",Width);
  InitMatrixKernel<<<size/1024,1024>>>(Nd,1e+10,size);
  getLastCudaError("InitMatrixKernel() execution failed.\n");
  dim3   DimGrid(Width/TILE_WIDTH,Width/TILE_WIDTH);  
  dim3   DimBlock(TILE_WIDTH,TILE_WIDTH); 

  for(int i=0;i<Level;i++)
  {
	printf("i=%d\n",i);
	MatrixMulKernel<<<DimGrid,DimBlock>>>( Md, Md, Nd, Width);
	cudaDeviceSynchronize();
	getLastCudaError("MatrixMulKernel() execution failed.\n");
	HANDLE_ERROR(cudaMemcpy(Md, Nd, size* sizeof(float),
		  cudaMemcpyDeviceToDevice));  
  }    
  // copy the array 'Nd' back from the GPU to the CPU
  HANDLE_ERROR( cudaMemcpy( Nh, Nd, size* sizeof(float),
		cudaMemcpyDeviceToHost ) );

  printf("All pair shortest paths.....................\n");	  	
  for(int i=0;i<Width;i++)
  {
	for(int j=0;j<Width;j++)
	{
	  if(Nh[i*Width+j]!=1e+10) 
		printf("%8.0f",Nh[i*Width+j]);
	  else
		printf("        ");    
	}
	printf("\n");
  }

  // free the memory allocated on the GPU
  HANDLE_ERROR( cudaFree( Md ) );
  HANDLE_ERROR( cudaFree( Nd ) );

  delete[] Mh;
  delete[] Nh;

  return 0;
}
